package com.onlineuctenka.settings;

/**
 * Class to set preference of app
 * 
 * @author redak105
 *
 */
public class Settings 
{
	/** url of remote PHP server */
	public static final String url ="http://www.online-uctenka.cz/login.android.php";
//	public static final String url ="http://10.0.2.2/Project_1/login.android.php";

	/** maximum size of side of picture */
	public static final float max_size = 240;
	
	/** compression of jpeg in range 0-100 */
	public static final int jpeg_compress = 20;
	
	/** url of remote PHP server */
	public static final String name_of_send_image ="image_from_adroid.jpeg";
	
}
