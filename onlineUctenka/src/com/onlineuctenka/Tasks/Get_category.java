package com.onlineuctenka.Tasks;



import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.onlineuctenka.R;
import com.onlineuctenka.classes.Category;
import com.onlineuctenka.php.Database;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.AdapterView.OnItemSelectedListener;

/**
 * Class to create and operate with dialog to select category  
 *
 * @author redak105
 *
 */
public class Get_category extends AsyncTask<Void, Void, Boolean> 
{
	/** Context from with we call this task */
	private Context context = null;
	/** Returning object from task */
	private JSONObject ret = null;
	/** Dialog to show list of categories */
	private Dialog dialog = null;
	
	/** Dialog to show loading */
	private ProgressDialog dialog_prog;
	
	/** Selected category from list */
	Category selected;
	
	/** instance of this class to create Simpleton */
	private static Get_category instance = null;
	
	ArrayList<Category> list;
	
	/**
	 * Call instance of this class
	 * @param ctx current context
	 * @return instance of this class
	 */
	public static Get_category getInstance(Context ctx) 
	{
        if (instance == null)
        {
        	instance = new Get_category(ctx);
        }
        return instance;
    }

	/**
	 * Destructor instance of class 
	 */
	public static void destructInstance() 
	{
        instance = null;
    }
	
	/**
	 * Get selected category from list
	 * @return class of selected category
	 */
	public static Boolean is_instance() {
		if (Get_category.instance != null)
		{
			return true;
		}
		return false; 
	}
	
	/**
	 * Get selected category from list
	 * @return class of selected category
	 */
	public Category get_selected() {
		return this.selected;
	}
	
	/**
	 * Show dialog to select category
	 */
	public void show()
	{
		this.dialog.show();
	}
	
	/**
	 * Constructor
	 * @param ctx current context
	 */
	public Get_category(Context ctx) {
		this.context = ctx;
    	
		// create new dialog to show
		this.dialog = new Dialog(this.context);
		this.dialog.setContentView(R.layout.dialog_category);
		this.dialog.setTitle(this.context.getResources().getString(R.string.save_category));
		this.dialog.setCancelable(true);
		this.dialog.setOnDismissListener(new OnDismissListener() {
			
			public void onDismiss(DialogInterface dialog) {
				// TODO Auto-generated method stub
				EditText b = (EditText) ((Activity) context).findViewById(R.bill_save.category_text);
				b.setText(selected.toString());
			}
		});
    	
    	Button select = (Button) this.dialog.findViewById(R.select_category.select);
    	select.setOnClickListener(new OnClickListener() {
			
    		@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
    			dialog.dismiss();
			}
		});
    	
    	this.dialog_prog = new ProgressDialog(this.dialog.getContext());
    }
	
	protected void onPreExecute(){ 
		super.onPreExecute();
		
		// show load.. dialog
		this.dialog_prog.setMessage(this.context.getResources().getString(R.string.task_category));
		this.dialog_prog.setCancelable(false);
		this.dialog_prog.show(); 
	}
	
    protected Boolean doInBackground(Void... params) {
    	// connector to database
    	Database db = Database.getInstance();
    	
    	// create parameters list
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add( new BasicNameValuePair("submit", "get_cat"));
		
		// send data
		this.ret = db.post(nameValuePairs);
		
		// test of success sending
		int succes;
		try {
			succes = this.ret.getInt("success");
			if (succes == 1)
			{
				parse_cat(ret);
				return true;
			}
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
					
		return false;
    }

    protected void onPostExecute(Boolean succes) {
    	dialog_prog.dismiss();
    	
    	// test to validate success of task
    	if (succes)
    	{
    		create();
    		this.dialog.show();
    	}
        
    }

	/**
	 * Parse data to list of categories
	 * @param ret return object from task to DB
	 * @throws JSONException
	 */
	public void parse_cat(JSONObject ret) throws JSONException
	{
		// create new array list of categories
		list = new ArrayList<Category>();
       
		// get array from object
		JSONArray array = ret.getJSONArray("categoryies");
		
		// search array
		for (int i=0; i<array.length(); i++)
		{
			// get object from array
			JSONObject obj = array.getJSONObject(i);
			
			// get parameters from object
			int id = obj.getInt("id");
			String name = obj.getString("name");
			JSONArray new_array = obj.getJSONArray("sub");
			
			// create new category
			Category cat = new Category(id, name);
			
			// search for subcategories
			get_subcategory( cat, new_array);
			
			// add category to list
			list.add(cat);
		}
		
        Log.i("log", "category finish");
	}
	
	/**
	 * Method to search and parse for subcategories of parent category
	 * @param category class of parent category
	 * @param array array of data to parse
	 * @throws JSONException
	 */
	private void get_subcategory(Category category, JSONArray array) throws JSONException
	{
		// test size of array
		if (array.length()>0)
		{
			// create lint to parent category
			Category cate = new Category( 0, this.dialog.getContext().getResources().getString(R.string.save_category_parent));
			category.add_category(cate);
		}
		else {
			return;
		}
		
		// search for subcategories
		for (int i=0; i<array.length(); i++)
		{
			// get object from array
			JSONObject obj = array.getJSONObject(i);
			
			// get parameters from object
			int id = obj.getInt("id");
			String name = obj.getString("name");
			
			// create new subcategory
			Category cat = new Category(id, name);
			
			// get array of subsubcategories
			JSONArray new_array = obj.getJSONArray("sub");
			
			// tes if this array exist
			if  (new_array != null)
			{
				// search for subsubcategories
				get_subcategory(cat, new_array);
			}
			
			// add subcategrory to parent category 
			category.add_category(cat);
			
			//Log.i("log", "list " + cat.toString());
		}
	}
	
	public void create() 
	{
		// find linear layout in dialog context 
		LinearLayout ll  = (LinearLayout) dialog.findViewById(R.select_category.category_layout);
		
		// create new spinner for main categories
		Spinner spinner_cat = new Spinner(ll.getContext());
		
//				LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
//					      LinearLayout.LayoutParams.MATCH_PARENT, 
//					      LinearLayout.LayoutParams.WRAP_CONTENT);
//				spinner_cat.setLayoutParams(lp);
		
		// fill spinner with data
		ArrayAdapter<Category> category_adapter = new ArrayAdapter<Category>(this.dialog.getContext(), android.R.layout.simple_spinner_item, list);
		category_adapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
		spinner_cat.setAdapter(category_adapter);
				
		// method to show other spinner related to selection 
		spinner_cat.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> adapter, View view, int position, long arg3) {
				
				// test if category is link to parent
				Category c = (Category) adapter.getSelectedItem();
				if (c.getId() == 0)
				{
					return;
				}
				
				// load list of subcategies
				ArrayList<Category> list = c.getList();
				
				// find linear layout in dialog context 
				LinearLayout ll  = (LinearLayout) dialog.findViewById(R.select_category.category_layout);
				
				// test size of this list
				if (list.size() != 0)
				{
					// create new spinner for subcategories
					Spinner spinner_cat = new Spinner(ll.getContext());
					
					// fill this spinner with data
					ArrayAdapter<Category> category_adapter = new ArrayAdapter<Category>(ll.getContext(), android.R.layout.simple_spinner_item, list);
					category_adapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
					spinner_cat.setAdapter(category_adapter);
					// add same listener to this spinner
					spinner_cat.setOnItemSelectedListener(this);
					
					// get parameters to delete old spinners
					int start = ll.indexOfChild((View) view.getParent()) + 1;
					int end = ll.getChildCount(); 
					int count = end - start;
					
//					Log.i("delete", "start " + start + " end " + end + " count " + count);
					
					// test if it can delete
					if ((start >= 0) && (count > 0))
					{
						// delete spinners
						ll.removeViews( start, count);
					}
					
					// add new sinner to layout
					ll.addView(spinner_cat);
				}
				else {
					// get parameters to delete old spinners
					int start = ll.indexOfChild((View) view.getParent()) + 1;
					int end = ll.getChildCount(); 
					int count = end - start;
					
//					Log.i("delete", "start " + start + " end " + end + " count " + count);
					
					// test if it can delete
					if ((start >= 0) && (count > 0))
					{
						// delete spinners
						ll.removeViews( start, count);
					}
				}
				
				// set selected file
				selected = c;
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
			}
			
		});
				
		// add main spinner
		ll.addView(spinner_cat);
	}
}
