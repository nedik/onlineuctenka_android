package com.onlineuctenka.Tasks;


import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.onlineuctenka.R;
import com.onlineuctenka.classes.User;
import com.onlineuctenka.php.Database;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.text.Editable;
import android.widget.EditText;
import android.widget.TextView;



/**
 * Class to provide add new company through AsyncTask 
 *
 * @author redak105
 *
 */
public class Add_company extends AsyncTask<Void, Void, Boolean> {
	
	/** Dialog from with we call this task */
	private Dialog dialog = null;
	/** Returning object from task */
	private JSONObject ret = null;
	/** Login user */
	private User user = null;
	
	/** Dialog to show loading */
	private ProgressDialog dialog_prog;
	
	/**
	 * Constructor
	 * @param dialog current dialog
	 * @param user class of login user
	 */
	public Add_company(Dialog dialog, User user) 
	{
		this.dialog = dialog;
		this.user = user;
		
		this.dialog_prog = new ProgressDialog(dialog.getContext());
    }
	
	protected void onPreExecute(){ 
	   super.onPreExecute();
	
	   // show load.. dialog
	   this.dialog_prog.setMessage(this.dialog.getContext().getResources().getString(R.string.task_company_set));
	   this.dialog_prog.setCancelable(false);
	   this.dialog_prog.show();    
	}
	
    protected Boolean doInBackground(Void... params) {
        // connector to database
    	Database db = Database.getInstance();
		
    	// create parameters list
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add( new BasicNameValuePair("submit", "set_comp"));
		nameValuePairs.add( new BasicNameValuePair("id_user", String.valueOf(user.getId())));
		
		EditText text = (EditText) this.dialog.findViewById(R.new_company.text);
		Editable company = text.getText();
		nameValuePairs.add( new BasicNameValuePair("name", company.toString()));
		
		// send data
		this.ret = db.post(nameValuePairs);
		
		// test of success sending
		int succes;
		try {
			succes = this.ret.getInt("success");
			if (succes == 1)
			{
				return true;
			}
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
					
		return false;
    }


    protected void onPostExecute(Boolean succes) {
    	this.dialog_prog.dismiss();
    	
    	// test to validate success of task 
    	if (succes)
    	{
        	this.dialog.dismiss();        	
    	}
    	else {
    		// show error message
    		TextView text = (TextView) dialog.findViewById(R.new_company.text_error);
    		try {
				text.setText(ret.getString("message"));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
        
    }
}