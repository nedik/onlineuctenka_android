package com.onlineuctenka;

import com.onlineuctenka.classes.Session_manager;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class FrontActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_front);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.front, menu);
		return true;
	}
	
	public void onStart() 
	{
		super.onStart();
		
		Session_manager session = new Session_manager(FrontActivity.this);
        Boolean login = session.check();
        
        if (login)
        {
        	Uri uri = Uri.parse("onlineUctenka://main");
	        Bundle bundle = new Bundle();
	        
	        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
	        
	        intent.putExtras(bundle);
	        startActivity(intent);
        }
	}
	
	public void onResume() 
	{
		super.onResume();
		
		Session_manager session = new Session_manager(FrontActivity.this);
        Boolean login = session.check();
        
        if (login)
        {
        	Uri uri = Uri.parse("onlineUctenka://main");
	        Bundle bundle = new Bundle();
	        
	        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
	        
	        intent.putExtras(bundle);
	        startActivity(intent);
        }
	}
	
	/**
	 * onClick action to start activity to login user
	 * @param view
	 */
	public void login(View view) 
	{    	        
	    Uri uri = Uri.parse("onlineUctenka://login");
	    Bundle bundle = new Bundle();
	    
	    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
	    
	    intent.putExtras(bundle);
	    startActivity(intent);
    }
	
	public boolean onOptionsItemSelected(MenuItem item) {
       
        switch (item.getItemId()) {
        	//delete
        	case R.menu_front.quit: {
        		// Closing all the Activities
        		finish();          
                moveTaskToBack(true);
        		return true;
        	}       	
        }
        return false;
    }

}
