package com.onlineuctenka.classes;
/**
 * Class to save information of company
 * 
 * @author redak105
 *
 */
public class Company 
{
	
	/** id of Company */
	private int id;
	/** name of Company */
	private String name;
	
	/**
	 * Constructor
	 * @param id id of new company
	 * @param name id of new company
	 */
	public Company(int id, String name) 
	{
		super();
		this.id = id;
		this.name = name;
	}

	/**
	 * Get id of Company 
	 * @return id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Get name of Company
	 * @return name of company
	 */
	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return  name;
	}
	
}
