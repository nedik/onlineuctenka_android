package com.onlineuctenka.classes;

import java.util.ArrayList;

/**
 * Class to save information of category
 * 
 * @author redak105
 *
 */
public class Category {

	/** id of category */
	private int id;
	/** name of category */
	private String name;
	
	/** list of child category */
	private ArrayList<Category> list = new ArrayList<Category>();
	
	/**
	 * Constructor
	 * @param id id of new category
	 * @param name name of new category
	 */
	public Category(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	/**
	 * Get id of category
	 * @return id 
	 */
	public int getId() {
		return id;
	}

	/**
	 * Get name of category
	 * @return name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Add category child 
	 * @param category child category
	 */
	public void add_category(Category category)
	{
		this.list.add(category);
	}
	
	/**
	 * Get list of category children
	 * @return list
	 */
	public ArrayList<Category> getList() {
		return list;
	}

	/**
	 * Get Category as {@link String}
	 * @return Name as {@link String}
	 */
	@Override
	public String toString() 
	{
		return this.name;
	}
}
