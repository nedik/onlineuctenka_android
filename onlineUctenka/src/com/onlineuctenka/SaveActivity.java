package com.onlineuctenka;

import java.io.File;
import java.util.Calendar;

import com.onlineuctenka.Tasks.Add_company;
import com.onlineuctenka.Tasks.Get_category;
import com.onlineuctenka.Tasks.Get_company;
import com.onlineuctenka.Tasks.Save_bill;
import com.onlineuctenka.classes.Session_manager;
import com.onlineuctenka.classes.User;
import com.onlineuctenka.php.Database;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnDismissListener;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.text.format.DateFormat;
import android.text.format.Time;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;

public class SaveActivity extends Activity {

	/** Dialog to pick date */
	private DatePickerDialog dialog_date;
	
	/** Dialog to add new company */
	private Dialog dialog_company;
	
	/** {@link AsyncTask} to get category */
	Get_category task_category = null;
	/** {@link AsyncTask} to get company */
	Get_company task_company_get = null;
	/** {@link AsyncTask} to add new company */
	Add_company new_comp_task = null;
	/** {@link AsyncTask} to save new bill */
	Save_bill save_task = null;
	
	/** class of login user */
	User user = null;
	/** class of session manager */
	Session_manager session = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_save);
		
		// check session
        this.session = new Session_manager(SaveActivity.this);
        this.session.check();
        
        // get login user
        this.user = session.get_user();
		
		final Calendar c = Calendar.getInstance();
	    // this year 
		int year = c.get(Calendar.YEAR);
		// this moth
	    int month = c.get(Calendar.MONTH);
	    // this day
	    int day = c.get(Calendar.DAY_OF_MONTH);
		
	    // Initialization of date picker dialog 
		this.dialog_date = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
        	@Override 
        	public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
	        {
        		// Initialization of current time
                Time date = new Time();
                date.set(dayOfMonth, monthOfYear, year);
                long dtDob = date.toMillis(true);
                CharSequence strDate = DateFormat.format("dd.MM.yyyy", dtDob);
                //set to text
                EditText text = (EditText)findViewById(R.bill_save.date_text);
                text.setText(strDate);
	        }}, year, month, day);
        this.dialog_date.setMessage(getResources().getString(R.string.save_date_dialog));
        
        // Initialization of add company dialog 
    	this.dialog_company = new Dialog(SaveActivity.this);    	
    	this.dialog_company.setContentView(R.layout.dialog_company);
    	this.dialog_company.setTitle(getResources().getString(R.string.save_company_new));
    	this.dialog_company.setCancelable(true);
    	this.dialog_company.setOnDismissListener(new OnDismissListener() {
			public void onDismiss(DialogInterface dialog) {
				// TODO Auto-generated method stub
				close_dialog_company();
			}
		});
    	
    	Button add = (Button) dialog_company.findViewById(R.new_company.button_add);
    	add.setOnClickListener(new OnClickListener() {
    		@Override
			public void onClick(View v) {
				new_comp_task = new Add_company( dialog_company, user);
				new_comp_task.execute((Void) null);
			}
		});
    	
    	Button cancel = (Button) dialog_company.findViewById(R.new_company.button_cancel);
    	cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog_company.dismiss();				
			}
		});
    	
    	// destruction of instance class
    	Get_category.destructInstance();
    	
    	// get list of user companies
    	task_company_get = new Get_company(this, this.user);
    	task_company_get.execute((Void) null);  
    	
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.save, menu);
		return true;
	}
	
	public void onStart() 
	{
		super.onStart();
		this.session = new Session_manager(SaveActivity.this);
        
        session.check();
	}
	
	public void onResume() 
	{		
		super.onResume();
		this.session = new Session_manager(SaveActivity.this);
        
        session.check();
        
        Boolean online = Database.isOnline(SaveActivity.this);
        if (!online)
        {
			show_error();
			return;
        }
	}
	
	/**
	 * Show error message for not connection
	 */
	private void show_error()
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(this);  
		builder.setTitle("You are not connect to internet");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {  
             @Override  
             public void onClick(DialogInterface dialog, int which) {  
                  dialog.cancel();
             }  
        });  
        AlertDialog alert = builder.create();  
        alert.show();
	}

	/**
	 * onClick action to show dialog to pick date
	 * @param view
	 */
	public void select_date(View view)
    {
    	this.dialog_date.show();
    }
	
	/**
	 * onClick action to show dialog to pick category
	 * @param view
	 */
	public void select_category(View view)
	{
		Boolean online = Database.isOnline(SaveActivity.this);
        if (!online)
        {
			show_error();
			return;
        }
        
        // get instance of class
        this.task_category = Get_category.getInstance( this);
        
        // test state of async task
        if (this.task_category.getStatus() == AsyncTask.Status.PENDING)
		{
        	this.task_category = Get_category.getInstance( this);
			this.task_category.execute((Void) null);
		}
		else {
			if (this.task_category.getStatus() == AsyncTask.Status.FINISHED)
			{
				this.task_category.show();
			}
		}		
	}
	
	/**
	 * onClick action to show dialog to create new company
	 * @param view
	 */
	public void new_company(View view)
	{
		Boolean online = Database.isOnline(SaveActivity.this);
        if (!online)
        {
			show_error();
			return;
        }
		
		this.dialog_company.show();
	}
	
	/**
	 * action witch close dialog to create new company
	 */
	public void close_dialog_company()
	{
		this.task_company_get = new Get_company(this, user);
		this.task_company_get.execute((Void) null);
    	
		this.dialog_company.dismiss();
	}
	
	/**
	 * onClick action to save bill to database
	 * @param view
	 */
	public void save(View view)
	{
		Boolean online = Database.isOnline(SaveActivity.this);
        if (!online)
        {
			show_error();
			return;
        }

        // test state of task
        if (this.task_category.getStatus() == AsyncTask.Status.FINISHED)
		{
        	save_task = new Save_bill(this, this.user);
    		save_task.execute((Void) null);
		}
        
//		save_task = new Save_bill(this, this.user);
//		save_task.execute((Void) null);
	}
	
	/** Address to image */
	private Uri image_uri;
	
	/** Id of create photo */
	static final int PHOTO = 1;
	static final int FILE = 2;

	/**
	 * Procedure to take photography from camera 
	 * @param view actual {@link View}
	 */
	public void take_photo(View view)
	{
	    Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
	    File photo = new File(Environment.getExternalStorageDirectory(),  "Foto_uctenka.jpg");
	    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
	    image_uri = Uri.fromFile(photo);
	    startActivityForResult(intent, PHOTO);
	}
	
	/**
	 * Procedure to pick image from memory
	 * @param view actual {@link View}
	 */
	public void show_file_chooser(View view) 
	{
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT); 
        intent.setType("image/*"); 
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        try {
            startActivityForResult(Intent.createChooser(intent, getResources().getString(R.string.save_image_file_dialog)), FILE);
        } catch (android.content.ActivityNotFoundException ex) {

        }
    }

	/**
	 * Get real path from {@link Uri}
	 * @param content_uri 
	 * @return path to image
	 */
    public String get_real_path(Uri content_uri) 
    {
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = getContentResolver().query(content_uri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String path = cursor.getString(column_index);
        cursor.close();
        return path;
    }

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	    super.onActivityResult(requestCode, resultCode, data);
	    
	    switch (requestCode) 
	    {
	    case PHOTO:
	        if (resultCode == Activity.RESULT_OK) 
	        {
	            Uri selectedImage = image_uri;
	            getContentResolver().notifyChange(selectedImage, null);
	            ImageView imageView = (ImageView) findViewById(R.bill_save.image_view);
	            ContentResolver cr = getContentResolver();
	            Bitmap bitmap;
	            try {
	            	// get new bitmap
	            	bitmap = android.provider.MediaStore.Images.Media.getBitmap(cr, selectedImage);
	                 
	            	// get old bitmap
	            	BitmapDrawable drawable = (BitmapDrawable) imageView.getDrawable();
	            	Bitmap oldBitmap = null;
	            	if (drawable != null)
	            	{
	            		oldBitmap = drawable.getBitmap();
	            		imageView.setImageDrawable(null);
	            	}
	            	// set new bitmap
	            	imageView.setImageBitmap(bitmap);
	            	// delete old bitmap
	            	if (oldBitmap != null)
	            	{
	            		oldBitmap.recycle();
	            	}
	                 
	            } catch (Exception e) {
	                Log.e("Camera", e.toString());
	            }
	            break;
	        }
	    case FILE:      
            if (resultCode == Activity.RESULT_OK) 
            {  
                Uri uri = data.getData();
                String path = get_real_path(uri);
                File imgFile = new File(path);
                ImageView imageView = (ImageView) findViewById(R.bill_save.image_view);
                Bitmap bitmap;
                if(imgFile.exists())
                {
                	// get new bitmap 
                   bitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                   
                // get old bitmap
	            	BitmapDrawable drawable = (BitmapDrawable) imageView.getDrawable();
	            	Bitmap oldBitmap = null;
	            	if (drawable != null)
	            	{
	            		oldBitmap = drawable.getBitmap();
	            		imageView.setImageDrawable(null);
	            	}
	            	// set new bitmap
	            	imageView.setImageBitmap(bitmap);
	            	// delete old bitmap
	            	if (oldBitmap != null)
	            	{
	            		oldBitmap.recycle();
	            	}
                }
                break;
            }       
	    }
	}	
	
	public boolean onOptionsItemSelected(MenuItem item) {
	       
        switch (item.getItemId()) {
        	//delete
        	case R.menu_save.quit: {
        		// Closing all the Activities
        		finish();          
                moveTaskToBack(true);
                return true;
        	}         	
        }
        return false;
    }

}
